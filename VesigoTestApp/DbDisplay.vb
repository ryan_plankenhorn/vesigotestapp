﻿Public Class DbDisplay
    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
    End Sub

    Private Sub DbDisplay_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim webResults As List(Of String)
        Try
            webResults = WebConnector.GetOrders()
        Catch ex As TimeoutException
            MessageBox.Show("The call to fetch orders from the web service has timed out.")
            Me.Close()
        Catch ex As ServiceModel.CommunicationException
            MessageBox.Show("Web service error occurred:\n" + ex.Message)
            Me.Close()
        End Try

        Try
            Using db As New DBHandler()
                db.writeOrderRecords(webResults)
                MessageBox.Show("Data loaded into the database at """ + db.Path + """", "Done")
                dgvOrders.DataSource = db.GetOrderDataset().Tables(0).AsDataView
            End Using
        Catch ex As SQLite.SQLiteException
            MessageBox.Show("Database error occurred:\n" + ex.Message)
            Me.Close()
        Catch ex As UnauthorizedAccessException
            MessageBox.Show("Unable to create database, access denied.")
            Me.Close()
        End Try

    End Sub
End Class
