﻿Imports System.Data.SQLite

Public Class DBHandler
    Implements IDisposable

    Public Const Path = "orders.sqlite"

    Dim conn As SQLiteConnection

    Sub New()
        SQLiteConnection.CreateFile(Path)

        conn = New SQLiteConnection("Data Source=" + Path)
        conn.Open()

        CreateTable()
    End Sub

    Public Function TableExists() As Boolean
        Try
            Using command As New SQLiteCommand(
            "SELECT Count(*) FROM Orders",
            conn)
                Dim reader As SQLiteDataReader
                reader = command.ExecuteReader()
                Return reader.HasRows
            End Using
        Catch ex As SQLiteException
            Return False
        End Try
    End Function

    Public Sub CreateTable()
        Using command As New SQLiteCommand(
            "CREATE TABLE Orders (orderNumber varchar(20))",
            conn)
            command.ExecuteNonQuery()
        End Using
    End Sub

    Public Sub writeOrderRecords(values As List(Of String))
        For Each value In values
            WriteOrderRecord(value)
        Next
    End Sub

    Public Sub WriteOrderRecord(value As String)
        Using command As New SQLiteCommand(
            "INSERT INTO Orders (orderNumber) VALUES (@number)",
            conn)
            command.Parameters.AddWithValue("@number", value)
            command.ExecuteNonQuery()
        End Using
    End Sub

    Public Function GetOrderDataset() As DataSet
        Dim data As New DataSet
        Using adapter As New SQLiteDataAdapter(
            "Select * From Orders",
            conn)
            adapter.Fill(data)
            Return data
        End Using
    End Function

    Private Sub IDisposable_Dispose() Implements IDisposable.Dispose
        conn.Dispose()
    End Sub
End Class
