﻿Public Class WebConnector
    Private Const SecurityKey = "29475d4e-34d8-4883-b6f0-f6836ddd60b4"
    Private Const Status = 2

    Public Shared Function GetOrders() As List(Of String)
        Using client As New OrdersReference.orders_internalSoapClient()
            Dim results As OrdersReference.ArrayOfString
            results = client.GetOrdersByStatus(SecurityKey,
                                           Status,
                                           SqlTypes.SqlDateTime.MinValue,
                                           SqlTypes.SqlDateTime.MaxValue)
            Return results.ToList()
        End Using
    End Function
End Class
